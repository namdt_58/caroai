package com.dev.thanhnamitit.caroai.models;

import com.dev.thanhnamitit.caroai.ui.Board;

import java.awt.*;

/**
 * Created by navi on 3/28/16.
 */
public class Area {

    private static final Color X_COLOR = Color.RED;
    private static final Color O_COLOR = Color.BLACK;

    private AreaKind kind;
    private Point point;

    public Area(int x, int y) {
        point = new Point(x, y);
        kind = AreaKind.none;
    }

    public void draw(Graphics2D g) {
        g.setFont(new Font("TimesRoman", Font.BOLD, Board.AREA_SIZE));

        if (kind == AreaKind.none) return;
        if (kind == AreaKind.X) {
            g.setColor(X_COLOR);
            g.drawString(kind.toString(), point.x * Board.AREA_SIZE + Board.AREA_SIZE / 7, (point.y + 1) * Board.AREA_SIZE - Board.AREA_SIZE / 7);

        } else {
            g.setColor(O_COLOR);
            g.drawString(kind.toString(), point.x * Board.AREA_SIZE + Board.AREA_SIZE / 10, (point.y + 1) * Board.AREA_SIZE - Board.AREA_SIZE / 7);

        }
    }

    public Area clone() {
        return new Area(point.x, point.y).setKind(kind);
    }

    public AreaKind getKind() {
        return kind;
    }

    public Area setKind(AreaKind kind) {
        this.kind = kind;
        return this;
    }

    public Point getPoint() {
        return this.point;
    }
    public String toString() {
        return point.toString();
    }

    public enum AreaKind {
        X, O, none;
    }

    public class Point {
        int x, y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public String toString() {
            return +x + " " + y;
        }
    }
}
