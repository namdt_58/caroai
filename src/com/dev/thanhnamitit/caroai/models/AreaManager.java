package com.dev.thanhnamitit.caroai.models;

import com.dev.thanhnamitit.caroai.ui.Board;

import java.awt.*;

/**
 * Created by navi on 3/28/16.
 */
public class AreaManager {

    private static final AreaManager INSTANCE = new AreaManager();

    public static AreaManager getInstance() {
        return INSTANCE;
    }

    private Area[][] areas;

    public static Area.AreaKind areaKindOfUser = Area.AreaKind.X, areaKindOfAI = Area.AreaKind.O;

    private AreaManagerCallBack callBack;

    private boolean gameOver = false;
    private Area startArea, endArea;


    private AreaManager() {
        areas = new Area[Board.NUMBER_AREA][Board.NUMBER_AREA];
        for (int i = 0; i < Board.NUMBER_AREA; i++)
            for (int j = 0; j < Board.NUMBER_AREA; j++)
                areas[i][j] = new Area(i, j);
    }

    public Area[][] getAreas() {
        return areas;
    }



    public void setCallBack(AreaManagerCallBack callBack) {
        this.callBack = callBack;
    }

    public void draw(Graphics2D g) {
        for (int i = 0; i < Board.NUMBER_AREA; i++)
            for (int j = 0; j < Board.NUMBER_AREA; j++)
                areas[i][j].draw(g);
        if (gameOver) drawLineWhenGameOver(g);
    }

    public void drawLineWhenGameOver(Graphics2D g) {
        int strokeSize = Board.AREA_SIZE / 10;
        g.setColor(Color.BLUE);
        g.setStroke(new BasicStroke(strokeSize));
        g.drawLine(startArea.getPoint().x * Board.AREA_SIZE + Board.AREA_SIZE / 2 - strokeSize / 2, startArea.getPoint().y * Board.AREA_SIZE + Board.AREA_SIZE / 2 - strokeSize / 2, endArea.getPoint().x * Board.AREA_SIZE + Board.AREA_SIZE / 2 - strokeSize / 2, endArea.getPoint().y * Board.AREA_SIZE + Board.AREA_SIZE / 2 - strokeSize / 2);
    }

    public void userPick(int x, int y) {
        Area area = areas[x][y];
        if (area.getKind() == Area.AreaKind.none) {
            area.setKind(areaKindOfUser);
            checkGameOver(area);
        } else callBack.tickOverlap();
    }

    public void aiPick(int x, int y) {
        Area area = areas[x][y];
        area.setKind(areaKindOfAI);
        checkGameOver(area);
    }


    public void checkGameOver(Area area) {
        int count = 0; // dem xem da co bao nhieu o thang hang
        int x = (int) area.getPoint().getX();
        int y = (int) area.getPoint().getY();

        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x + i][y + i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                if (++count == 5) {
                    gameOver = true;
                    endArea = areaToCheck;
                    startArea = areas[areaToCheck.getPoint().x - 4][areaToCheck.getPoint().y - 4];
                    callBack.endGame();
                    return;
                }
            } else {
                count = 0;
            }
        }
        count = 0;
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x + i][y - i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                if (++count == 5) {
                    gameOver = true;
                    endArea = areaToCheck;
                    startArea = areas[areaToCheck.getPoint().x - 4][areaToCheck.getPoint().y + 4];
                    callBack.endGame();
                    return;
                }
            } else {
                count = 0;
            }
        }
        count = 0;
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x + i][y];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                if (++count == 5) {
                    gameOver = true;
                    endArea = areaToCheck;
                    startArea = areas[areaToCheck.getPoint().x - 4][areaToCheck.getPoint().y];
                    callBack.endGame();
                    return;
                }
            } else {
                count = 0;
            }
        }
        count = 0;
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x][y + i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                if (++count == 5) {
                    gameOver = true;
                    endArea = areaToCheck;
                    startArea = areas[areaToCheck.getPoint().x][areaToCheck.getPoint().y - 4];
                    callBack.endGame();
                    return;
                }
            } else {
                count = 0;
            }
        }
        if (area.getKind() == areaKindOfUser)
            callBack.tickSuccess(area);
    }

    public void resetGame() {
        gameOver = false;
        for (int i = 0; i < Board.NUMBER_AREA; i++)
            for (int j = 0; j < Board.NUMBER_AREA; j++)
                areas[i][j].setKind(Area.AreaKind.none);
    }


    public interface AreaManagerCallBack {
        void tickOverlap();

        void tickSuccess(Area area);

        void endGame();

    }
}
