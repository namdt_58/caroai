package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.models.Area;
import com.dev.thanhnamitit.caroai.models.AreaManager;

import java.util.ArrayList;

/**
 * Created by navi on 3/29/16.
 */
public class NormalAI extends StupidAI { // Biet tim de chan va danh cac nuoc doi

    private int maxVertical, minVertical, maxHorizontal, minHorizontal;


    @Override
    public Area findNextArea() {

//        ArrayList<Area> listAreaDoubleWayOfUser = findDoubleWay(false); // tim cua USER
//        Area doubleWayToWinOfUser = analyzeDoubleWay(listAreaDoubleWayOfUser, false);
//        if (doubleWayToWinOfUser != null) return doubleWayToWinOfUser;
        Area result = findNextAreaToDoNotNeedTickRandom();
        if (result != null) return result;
        return tickRandom(listAreaOfUser.get(listAreaOfUser.size() - 1));
        //  return new Area(1, 1);// tickRandom(listAreaOfUser.get(listAreaOfUser.size() - 1));
    }

    @Override
    protected Area findNextAreaToDoNotNeedTickRandom() {

        ArrayList<AreaAI> listAreaCanTickForAIWin = findListAreaCanTickToWin();

        for (AreaAI areaAI : listAreaCanTickForAIWin) { // neu AI co nuoc thang luon thi danh luon
            if (areaAI.threatDegree == FIRST) return areaAI;
        }
        ArrayList<AreaAI> listAreaCanTickToBlockUserWin = findListAreaCanTickToBlockUserWin();
        for (AreaAI areaAI : listAreaCanTickToBlockUserWin) { // neu user co nuoc de thang luon thi phai chan
            if (areaAI.threatDegree == FIRST) return areaAI;
        }
        if (listAreaCanTickForAIWin.size() != 0) { // neu AI có nước để dẫn đến thắng thì đánh luôn
            return listAreaCanTickForAIWin.get(random.nextInt(listAreaCanTickForAIWin.size()));
        }
        ArrayList<Area> listAreaDoubleWayOfAI = findDoubleWay(true); // tim cho AI
        ArrayList<Area> listAreaDoubleWayOfUser = findDoubleWay(false); // tim cua USER
        ArrayList<Area> listAreaAICanTickForUserBlock = findListAreaForBlock(true); // tim danh sach danh de bat buoc nguoi choi phai do
        ArrayList<Area> listAreaUserCanTickForAIBlock = findListAreaForBlock(false); // tim danh sach danh de bat buoc AI phai do
        ArrayList<Area> listAreaSimilarities1 = findSimilaritiesIn2ListArea(listAreaDoubleWayOfAI, listAreaCanTickToBlockUserWin);
        if (listAreaSimilarities1.size() > 0) {
            System.out.println("1");
            return listAreaSimilarities1.get(0);
        }
        ArrayList<Area> listAreaSimilarities2 = findSimilaritiesIn2ListArea(listAreaAICanTickForUserBlock, listAreaCanTickToBlockUserWin);
        if (listAreaSimilarities2.size() > 0) {
            System.out.println("2");
            return listAreaSimilarities2.get(0);
        }
        ArrayList<Area> listAreaSimilarities3 = findSimilaritiesIn2ListArea(listAreaAICanTickForUserBlock, listAreaDoubleWayOfUser, false);
        if (listAreaSimilarities3.size() > 0) {
            System.out.println("3");
            return listAreaSimilarities3.get(0);
        }
        if (listAreaCanTickToBlockUserWin.size() != 0) {
            System.out.println("4");
            return listAreaCanTickToBlockUserWin.get(random.nextInt(listAreaCanTickToBlockUserWin.size()));
        }
        Area doubleWayToWinOfAi = analyzeDoubleWay(listAreaDoubleWayOfAI, true);
        if (doubleWayToWinOfAi != null) return doubleWayToWinOfAi;
        Area doubleWayToWinOfUser = analyzeDoubleWay(listAreaDoubleWayOfUser, false);
        if (doubleWayToWinOfUser != null) return doubleWayToWinOfUser;
        if (listAreaDoubleWayOfAI.size() != 0) {
            System.out.println("5");
            return listAreaDoubleWayOfAI.get(random.nextInt(listAreaDoubleWayOfAI.size()));
        }
        if (listAreaDoubleWayOfUser.size() != 0) {
            System.out.println("6");
            return listAreaDoubleWayOfUser.get(random.nextInt(listAreaDoubleWayOfUser.size()));
        }
        ArrayList<Area> listAreaSimilarities4 = findSimilaritiesIn2ListArea(listAreaUserCanTickForAIBlock, listAreaAICanTickForUserBlock, false);
        if (listAreaSimilarities4.size() != 0) {
            System.out.println("9");
            return listAreaSimilarities4.get(random.nextInt(listAreaSimilarities4.size()));
        }
        if (listAreaAICanTickForUserBlock.size() != 0) {
            System.out.println("7");
            return listAreaAICanTickForUserBlock.get(random.nextInt(listAreaAICanTickForUserBlock.size()));
        }
        System.out.println("8");
        return null;
    }

    public Area analyzeDoubleWay(ArrayList<Area> areas, boolean findForAI) {
        for (Area area : areas) {
            if (findForAI) area.setKind(AreaManager.areaKindOfAI);
            else area.setKind(AreaManager.areaKindOfUser);
            ArrayList<AreaAI> areaAIs = analyzeArea(area);
            for (AreaAI areaAI : areaAIs) {
                if (areaAI.threatDegree == FIRST) {
                    area.setKind(Area.AreaKind.none);
                    return area;
                }
            }
            area.setKind(Area.AreaKind.none);
        }
        return null;
    }

    public ArrayList<Area> findSimilaritiesIn2ListArea(ArrayList<Area> list1, ArrayList<AreaAI> list2) {
        ArrayList<Area> result = new ArrayList<>();
        for (Area area1 : list1) {
            for (Area area2 : list2) {
                if (area1.equals(area2)) {
                    result.add(area1);
                }
            }
        }
        return result;
    }

    public ArrayList<Area> findSimilaritiesIn2ListArea(ArrayList<Area> list1, ArrayList<Area> list2, boolean a) {
        ArrayList<Area> result = new ArrayList<>();
        for (Area area1 : list1) {
            for (Area area2 : list2) {
                if (area1.equals(area2)) {
                    result.add(area1);
                }
            }
        }
        return result;
    }

    @Override
    protected ArrayList<AreaAI> findListAreaCanTickToBlockUserWin() {
        ArrayList<AreaAI> listAreaCanTickToBlockUserWin = new ArrayList<>();
        for (Area area : listAreaOfUser) {
            listAreaCanTickToBlockUserWin = addListToNewListWithoutEquals(listAreaCanTickToBlockUserWin, analyzeArea(area));

        }
        return listAreaCanTickToBlockUserWin;
    }

    @Override
    public void addAreaToListAreaOfUser(Area area) {
        super.addAreaToListAreaOfUser(area);
        checkLimit(area);
    }

    @Override
    public void addAreaToListAreaOfAI(Area area) {
        super.addAreaToListAreaOfAI(area);
        checkLimit(area);
    }

    private void checkLimit(Area area) {
        if (listAreaOfUser.size() + listAreaOfAI.size() == 0) {
            maxHorizontal = (int) area.getPoint().getX();
            minHorizontal = (int) area.getPoint().getX();
            maxVertical = (int) area.getPoint().getY();
            minHorizontal = (int) area.getPoint().getY();
        } else {
            if (maxHorizontal < area.getPoint().getX()) maxHorizontal = (int) area.getPoint().getX();
            if (minHorizontal > area.getPoint().getX()) maxHorizontal = (int) area.getPoint().getX();
            if (maxVertical < area.getPoint().getX()) maxVertical = (int) area.getPoint().getY();
            if (minVertical > area.getPoint().getX()) minVertical = (int) area.getPoint().getY();
        }
    }

    public ArrayList<Area> findListAreaForBlock(boolean findForAI) {
        ArrayList<Area> result = new ArrayList<>();
        Area check;
        for (int i = minHorizontal - 2; i <= maxHorizontal + 2; i++) {
            for (int j = minVertical - 2; j <= maxVertical + 2; j++) {
                try {
                    check = areas[i][j];
                    if (check.getKind() != Area.AreaKind.none) continue;
                    if (findForAI)
                        check.setKind(AreaManager.areaKindOfAI);
                    else check.setKind(AreaManager.areaKindOfUser);
                    if (countNumberKindBlockAtArea(check) > 0) {
                        result.add(check);
                    }
                    check.setKind(Area.AreaKind.none);
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }

        }
        return result;
    }

    public ArrayList<Area> findDoubleWay(boolean findForAI) {
        ArrayList<Area> result = new ArrayList<>();
        Area check;
        for (int i = minHorizontal - 2; i <= maxHorizontal + 2; i++) {
            for (int j = minVertical - 2; j <= maxVertical + 2; j++) {
                try {
                    check = areas[i][j];
                    if (check.getKind() != Area.AreaKind.none) continue;
                    if (findForAI) {
                        check.setKind(AreaManager.areaKindOfAI);
                    } else {
                        check.setKind(AreaManager.areaKindOfUser);
                    }
                    if (countNumberKindBlockAtArea(check) > 1) {
                        result.add(check);
                    }
                    check.setKind(Area.AreaKind.none);
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        }
        return result;
    }

    private int countNumberKindBlockAtArea(Area area) {
        int count = 0;
        if (analyzeHorizontal(area).size() > 0) count++;
        if (analyzeVertical(area).size() > 0) count++;
        if (analyzeMainDiagonal(area).size() > 0) count++;
        if (analyzeExtraDiagonal(area).size() > 0) count++;
        return count;
    }
}
