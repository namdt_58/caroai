package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.models.Area;
import com.dev.thanhnamitit.caroai.models.AreaManager;

/**
 * Created by navi on 3/29/16.
 */
public interface Actions {
    Area findNextArea();
    void addAreaToListAreaOfUser(Area area);
    void addAreaToListAreaOfAI(Area area);
    void reset();
}
