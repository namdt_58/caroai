package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.models.Area;
import com.dev.thanhnamitit.caroai.old.Brain;
import com.dev.thanhnamitit.caroai.old.Caro;
import com.dev.thanhnamitit.caroai.ui.Board;

/**
 * Created by TooNies1810 on 4/12/16.
 */
public class SmartAI extends NormalAI {

    private boolean matrixX[][] = new boolean[Board.NUMBER_AREA][Board.NUMBER_AREA];
    private boolean matrixO[][]= new boolean[Board.NUMBER_AREA][Board.NUMBER_AREA];

    private Brain brain = new Brain(Board.NUMBER_AREA, Board.NUMBER_AREA);

    private Area analyzeToTickRandom() {
        if (listAreaOfUser.size() == 1){
            listAreaOfAI.clear();
        }
        matrixX = new boolean[Board.NUMBER_AREA][Board.NUMBER_AREA];
        matrixO = new boolean[Board.NUMBER_AREA][Board.NUMBER_AREA];

        brain = new Brain(Board.NUMBER_AREA, Board.NUMBER_AREA);

        System.out.println("ai size: " + listAreaOfAI.size());
        System.out.println("user size: " + listAreaOfUser.size());

        for (int i = 0; i < matrixO[1].length; i++) {
            for (int j = 0; j < matrixO.length; j++) {
                matrixO[i][j] = false;
                matrixX[i][j] = false;
            }
        }
        //convert to old prj
        for (int i = 0; i < listAreaOfAI.size(); i++) {
            int x = (int) listAreaOfAI.get(i).getPoint().getX();
            int y = (int) listAreaOfAI.get(i).getPoint().getY();
//            System.out.println("ai: " + x +"" + y);
            matrixO[x][y] = true;
        }

        for (int i = 0; i < listAreaOfUser.size(); i++) {
            int x = (int) listAreaOfUser.get(i).getPoint().getX();
            int y = (int) listAreaOfUser.get(i).getPoint().getY();
//            System.out.println("user: " + x +"" + y);
            matrixX[x][y] = true;
        }

        Caro caro = brain.nextCaro(matrixO, matrixX);
        Area area = new Area(caro.getX(),caro.getY());
        area.setKind(Area.AreaKind.O);



        return area;
    }

    @Override
    protected Area tickRandom(Area area) {
        if (listAreaOfAI.size() < 1) return super.tickRandom(area);
        else return analyzeToTickRandom();
    }
}
