package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.models.Area;

import java.util.ArrayList;

/**
 * Created by thanh_000 on 3/30/2016.
 */
public interface HandlingCase {
    ArrayList<AreaAI> LIST_POINT_TO_BLOCK = new ArrayList<>();
    ArrayList<AreaAI> checkTwoPoint(Area area); // kiem tra khi 2 diem thang hang
    ArrayList<AreaAI> checkThreePoint(Area area); // kiem tra khi 3 diem nam thang hang
    ArrayList<AreaAI> checkFourPoint(Area area); // kiem tra khi 4 diem thang hang


}
