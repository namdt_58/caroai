package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.base.BaseAI;
import com.dev.thanhnamitit.caroai.models.Area;

import java.util.ArrayList;

/**
 * Created by navi on 3/29/16.
 */
public class StupidAI extends BaseAI { // Thằng này chỉ biết chặn nước đi đơn va danh cac nuoc quyet dinh de win


    @Override
    public Area findNextArea() {

        Area result = findNextAreaToDoNotNeedTickRandom();
        if (result != null) return result;
        return tickRandom(listAreaOfUser.get(listAreaOfUser.size() - 1));
    }

    protected Area findNextAreaToDoNotNeedTickRandom() {
        ArrayList<AreaAI> listAreaCanTickToBlockUserWin = findListAreaCanTickToBlockUserWin();
        ArrayList<AreaAI> listAreaCanTickForAIWin = findListAreaCanTickToWin();
        for (AreaAI areaAI : listAreaCanTickForAIWin) {
            if (areaAI.threatDegree == FIRST) return areaAI;
        }
        for (AreaAI areaAI : listAreaCanTickToBlockUserWin) {
            if (areaAI.threatDegree == FIRST) return areaAI;
        }
        if (listAreaCanTickForAIWin.size() != 0) {
            return listAreaCanTickForAIWin.get(random.nextInt(listAreaCanTickForAIWin.size()));
        }
        if (listAreaCanTickToBlockUserWin.size() != 0) {
            return listAreaCanTickToBlockUserWin.get(random.nextInt(listAreaCanTickToBlockUserWin.size()));
        }
        return null;
    }

    protected ArrayList<AreaAI> findListAreaCanTickToWin() {
        ArrayList<AreaAI> listAreaCanTickForAIWin = new ArrayList<>();
        for (Area area : listAreaOfAI) {
            ArrayList<AreaAI> result = analyzeArea(area);
            for (AreaAI area1 : result) {
                listAreaCanTickForAIWin.add(area1);
            }
        }
        return listAreaCanTickForAIWin;
    }

    protected ArrayList<AreaAI> findListAreaCanTickToBlockUserWin() {
       // ArrayList<AreaAI> listAreaCanTickToBlockUserWin = new ArrayList<>();
        Area previousAreaUserPick = listAreaOfUser.get(listAreaOfUser.size() - 1);
        return analyzeArea(previousAreaUserPick);
    }

    protected ArrayList<AreaAI> analyzeArea(Area area) {
        ArrayList<AreaAI> result;
        result = addListToNewListWithoutEquals(analyzeHorizontal(area), analyzeVertical(area));
        result = addListToNewListWithoutEquals(result, analyzeMainDiagonal(area));
        result = addListToNewListWithoutEquals(result, analyzeExtraDiagonal(area));
        return result;
    }

    public ArrayList<AreaAI> addListToNewListWithoutEquals(ArrayList<AreaAI> list, ArrayList<AreaAI> result) {
        for (AreaAI areaAI : list) {
            boolean check = false;
            for (AreaAI areaAI1 : result) {
                if (areaAI.equals(areaAI1)) {
                    check = true;
                    break;
                }
            }
            if (!check) result.add(areaAI);
        }
        return result;
    }

    public ArrayList<AreaAI> analyzeVertical(Area area) {
        ArrayList<AreaAI> result = new ArrayList<>();
        int count = 0;
        int x = (int) area.getPoint().getX();
        int y = (int) area.getPoint().getY();
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x][y + i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                count++;
                if (count == 2) {
                    addListToNewListWithoutEquals(verticalCase.checkTwoPoint(areaToCheck), result);
                } else if (count == 3) {
                    addListToNewListWithoutEquals(verticalCase.checkThreePoint(areaToCheck), result);
                } else if (count == 4) {
                    addListToNewListWithoutEquals(verticalCase.checkFourPoint(areaToCheck), result);
                }
            } else {
                count = 0;
            }
        }

        return result;
    }

    public ArrayList<AreaAI> analyzeHorizontal(Area area) {
        ArrayList<AreaAI> result = new ArrayList<>();
        int count = 0;
        int x = (int) area.getPoint().getX();
        int y = (int) area.getPoint().getY();
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x + i][y];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                count++;
                if (count == 2) {
                    addListToNewListWithoutEquals(horizontalCase.checkTwoPoint(areaToCheck), result);
                } else if (count == 3) {
                    addListToNewListWithoutEquals(horizontalCase.checkThreePoint(areaToCheck), result);
                } else if (count == 4) {
                    addListToNewListWithoutEquals(horizontalCase.checkFourPoint(areaToCheck), result);
                }
            } else {
                count = 0;
            }
        }
        return result;

    }

    public ArrayList<AreaAI> analyzeMainDiagonal(Area area) {
        ArrayList<AreaAI> result = new ArrayList<>();
        int count = 0;
        int x = (int) area.getPoint().getX();
        int y = (int) area.getPoint().getY();
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x + i][y + i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                count++;
                if (count == 2) {
                    addListToNewListWithoutEquals(mainDiagonalCase.checkTwoPoint(areaToCheck), result);
                } else if (count == 3) {
                    addListToNewListWithoutEquals(mainDiagonalCase.checkThreePoint(areaToCheck), result);
                } else if (count == 4) {
                    addListToNewListWithoutEquals(mainDiagonalCase.checkFourPoint(areaToCheck), result);
                }
            } else {
                count = 0;
            }
        }
        return result;
    }

    public ArrayList<AreaAI> analyzeExtraDiagonal(Area area) {
        ArrayList<AreaAI> result = new ArrayList<>();
        int count = 0;
        int x = (int) area.getPoint().getX();
        int y = (int) area.getPoint().getY();
        for (int i = -4; i <= 4; i++) {
            Area areaToCheck;
            try {
                areaToCheck = areas[x - i][y + i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
            if (areaToCheck.getKind() == area.getKind()) {
                count++;
                if (count == 2) {
                    addListToNewListWithoutEquals(extraDiagonalCase.checkTwoPoint(areaToCheck), result);
                } else if (count == 3) {
                    addListToNewListWithoutEquals(extraDiagonalCase.checkThreePoint(areaToCheck), result);
                } else if (count == 4) {
                    addListToNewListWithoutEquals(extraDiagonalCase.checkFourPoint(areaToCheck), result);
                }
            } else {
                count = 0;
            }
        }
        return result;
    }

}
