package com.dev.thanhnamitit.caroai.ai;

import com.dev.thanhnamitit.caroai.models.Area;

/**
 * Created by thanh_000 on 3/31/2016.
 */
public class AreaAI extends Area {
    int threatDegree;

    public AreaAI(int x, int y, int threatDegree) {
        super(x, y);
        this.threatDegree = threatDegree;
    }

    public AreaAI(Area area, int threatDegree) {
        super((int)area.getPoint().getX(), (int)area.getPoint().getY());
        this.threatDegree = threatDegree;
    }

    public boolean equals(Area area) {
        return getPoint().getX() == area.getPoint().getX() && getPoint().getY() == area.getPoint().getY();
    }
}
