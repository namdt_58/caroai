package com.dev.thanhnamitit.caroai.ui;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by thanh_000 on 4/20/2016.
 */
public class GameMenu extends JPanel {
    private JButton easy, normal, hard;
    private OnClickListener listener;
    public GameMenu() {
        setLayout(null);
        initComponent();
    }

    private void initComponent() {
        easy = new JButton();
        easy.setText("Easy");
        easy.setBounds(10, 10, 250, 50);
        add(easy);
        normal = new JButton();
        normal.setText("Normal");
        normal.setBounds(10, 70, 250, 50);
        add(normal);
        hard = new JButton();
        hard.setText("Hard");
        hard.setBounds(10,130,250,50);
        add(hard);
        easy.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                listener.onClick(Board.Mode.Easy);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        normal.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                listener.onClick(Board.Mode.Normal);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        hard.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                listener.onClick(Board.Mode.Hard);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    public void setOnClick(OnClickListener onClick)
    {
        this.listener = onClick;
    }

    public interface OnClickListener{
        void onClick(Board.Mode mode);
    }
}
