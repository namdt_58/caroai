package com.dev.thanhnamitit.caroai.ui;

import com.dev.thanhnamitit.caroai.ai.Actions;
import com.dev.thanhnamitit.caroai.ai.NormalAI;
import com.dev.thanhnamitit.caroai.ai.SmartAI;
import com.dev.thanhnamitit.caroai.ai.StupidAI;
import com.dev.thanhnamitit.caroai.models.Area;
import com.dev.thanhnamitit.caroai.models.AreaManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by navi on 3/28/16.
 */
public class Board extends JPanel implements MouseListener, MouseMotionListener, AreaManager.AreaManagerCallBack {
    public static final int NUMBER_AREA = 30;
    public static int AREA_SIZE;
    private int xRect, yRect;
    private AreaManager areaManager = AreaManager.getInstance();

    private Actions aiActions = new SmartAI();

    public Board() {
        setSize(Gui.GUI_SIZE, Gui.GUI_SIZE);
        AREA_SIZE = Gui.GUI_SIZE / NUMBER_AREA;
        addMouseListener(this);
        addMouseMotionListener(this);
        areaManager.setCallBack(this);
    }

    public void setMode(Mode mode) {
        switch (mode) {
            case Easy:
                aiActions = new StupidAI();
                break;
            case Normal:
                aiActions = new NormalAI();
                break;
            case Hard:
                aiActions = new SmartAI();
                break;
        }
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        drawBoard(g2d);
        drawRectangle(g2d);
        areaManager.draw(g2d);
    }

    public void drawBoard(Graphics2D g) {
        g.setColor(Color.BLUE);
        for (int i = 0; i <= NUMBER_AREA; i++) {
            g.drawLine(0, i * AREA_SIZE, Gui.GUI_SIZE, i * AREA_SIZE);
            g.drawLine(i * AREA_SIZE, 0, i * AREA_SIZE, Gui.GUI_SIZE);
        }
    }

    public void drawRectangle(Graphics2D g) // ve hinh chu nhat o vi tri con chuot
    {
        g.setColor(Color.RED);
        g.drawRect(xRect, yRect, AREA_SIZE, AREA_SIZE);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int x = mouseEvent.getX() / AREA_SIZE;
        int y = mouseEvent.getY() / AREA_SIZE;
        areaManager.userPick(x, y);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        xRect = (mouseEvent.getX() / AREA_SIZE) * AREA_SIZE;
        yRect = (mouseEvent.getY() / AREA_SIZE) * AREA_SIZE;
        repaint();
    }

    @Override
    public void tickSuccess(Area area) {
        aiActions.addAreaToListAreaOfUser(area.setKind(AreaManager.areaKindOfUser));
        repaint();
        Area areaAiPick = aiActions.findNextArea();
        areaManager.aiPick(areaAiPick.getPoint().getX(), areaAiPick.getPoint().getY());
        aiActions.addAreaToListAreaOfAI(areaAiPick.setKind(AreaManager.areaKindOfAI));
    }

    @Override
    public void tickOverlap() {

    }


    @Override
    public void endGame() {
        repaint();
        JOptionPane.showMessageDialog(null, "Game over!!!");
        areaManager.resetGame();
        aiActions.reset();
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    public enum Mode {
        Hard, Normal, Easy
    }

}
