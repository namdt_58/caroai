package com.dev.thanhnamitit.caroai.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by navi on 3/28/16.
 */
public class Gui extends JFrame implements GameMenu.OnClickListener {
    private static final String TITLE = "Caro AI";

    public static int GUI_SIZE;
    private Board board;
    private GameMenu gameMenu;
    public Gui() {
        setTitle(TITLE);
        int w = Toolkit.getDefaultToolkit().getScreenSize().width;
        int h = Toolkit.getDefaultToolkit().getScreenSize().height;
        GUI_SIZE = h*4/5;
        //   setBounds((w-900)/2, 0, 980, 701);
        setSize(GUI_SIZE, GUI_SIZE);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        gameMenu = new GameMenu();
        gameMenu.setOnClick(this);
        board = new Board();
        add(board);
        add(gameMenu);
        board.setVisible(false);
    }

    @Override
    public void onClick(Board.Mode mode) {
        board.setMode(mode);
        gameMenu.setVisible(false);
        board.setVisible(true);
    }
}
