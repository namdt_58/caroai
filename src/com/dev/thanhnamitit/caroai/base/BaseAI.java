package com.dev.thanhnamitit.caroai.base;

import com.dev.thanhnamitit.caroai.ai.Actions;
import com.dev.thanhnamitit.caroai.ai.AreaAI;
import com.dev.thanhnamitit.caroai.ai.HandlingCase;
import com.dev.thanhnamitit.caroai.models.Area;
import com.dev.thanhnamitit.caroai.models.AreaManager;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by navi on 3/29/16.
 */
public abstract class BaseAI implements Actions {

    protected static final int FIRST = 1;
   // protected static final int SECOND = 2;
   // protected static final int THIRRD = 3;

    protected static final int FOURTH = 4;


    protected AreaManager areaManger = AreaManager.getInstance();
    protected Area[][] areas;
    protected Random random = new Random();
    protected ArrayList<Area> listAreaOfUser = new ArrayList<>();
    protected ArrayList<Area> listAreaOfAI = new ArrayList<>();
    protected HandlingCase horizontalCase = new HandlingCase() {
        @Override
        public ArrayList<AreaAI> checkTwoPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {

                 /*
                *    txtxxt
                *        ^
                * */
                if (areas[x - 2][y].getKind() == Area.AreaKind.none && areas[x - 3][y].getKind() == area.getKind() && areas[x - 4][y].getKind() == Area.AreaKind.none && areas[x + 1][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 2][y], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *   txxtxt
                *     ^
                * */
                if (areas[x + 1][y].getKind() == Area.AreaKind.none && areas[x + 2][y].getKind() == area.getKind() && areas[x + 3][y].getKind() == Area.AreaKind.none && areas[x - 2][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    xxtxx
                *     ^
                * */
                if (areas[x + 3][y].getKind() == area.getKind() && areas[x + 2][y].getKind() == area.getKind() && areas[x + 1][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FIRST));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkThreePoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                /*
                *    xtxxx
                *        ^
                * */
                if (areas[x - 4][y].getKind() == area.getKind() && areas[x - 3][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y], FIRST));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    xxxtx
                *      ^
                * */
                if (areas[x + 2][y].getKind() == area.getKind() && areas[x + 1][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FIRST));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }

            try {
                /*
                *    ttxxxtt
                *        ^
                * */
                if (areas[x - 3][y].getKind() == Area.AreaKind.none && areas[x - 4][y].getKind() == Area.AreaKind.none && areas[x + 1][y].getKind() == Area.AreaKind.none && areas[x + 2][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FOURTH));
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    ttxxxt
                *        ^
                * */
                if (areas[x - 3][y].getKind() == Area.AreaKind.none && areas[x - 4][y].getKind() == Area.AreaKind.none && areas[x + 1][y].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    txxxtt
                *       ^
                * */
                if (areas[x - 3][y].getKind() == Area.AreaKind.none && areas[x + 1][y].getKind() == Area.AreaKind.none && areas[x + 2][y].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }


            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkFourPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            /*
                *    txxxx
                *        ^
                * */
            try {
                if (areas[x - 4][y].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 4][y], FIRST));

            } catch (ArrayIndexOutOfBoundsException e) {
            }
             /*
                *    xxxxt
                *       ^
                * */
            try {
                if (areas[x + 1][y].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y], FIRST));

            } catch (ArrayIndexOutOfBoundsException e) {
            }
            return LIST_POINT_TO_BLOCK;
        }
    };
    protected HandlingCase verticalCase = new HandlingCase() {
        @Override
        public ArrayList<AreaAI> checkTwoPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                /*
                *
                *  t
                *  x
                *  x <
                *  t
                *  x
                *  t
                *
                * */

                if (areas[x][y - 2].getKind() == Area.AreaKind.none && areas[x][y + 1].getKind() == Area.AreaKind.none && areas[x][y + 2].getKind() == area.getKind() && areas[x][y + 3].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FOURTH));

            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *
                *  t
                *  x
                *  t
                *  x
                *  x <
                *  t
                *
                * */

                if (areas[x][y - 4].getKind() == Area.AreaKind.none && areas[x][y - 2].getKind() == Area.AreaKind.none && areas[x][y - 3].getKind() == area.getKind() && areas[x][y + 1].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y - 2], FOURTH));

            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *
                *  x
                *  x <
                *  t
                *  x
                *  x
                *
                * */

                if (areas[x][y + 1].getKind() == Area.AreaKind.none && areas[x][y + 2].getKind() == area.getKind() && areas[x][y + 3].getKind() == area.getKind())
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FIRST));

            } catch (ArrayIndexOutOfBoundsException e) {

            }
            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkThreePoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                /*
                * x
                * t
                * x
                * x
                * x <
                * */
                if (areas[x][y - 4].getKind() == area.getKind() && areas[x][y - 3].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y - 3], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                /*
                * x
                * x
                * x <
                * t
                * x
                * */
                if (areas[x][y + 2].getKind() == area.getKind() && areas[x][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                /*
                * t
                * t
                * x
                * x
                * x <
                * t
                * t
                * */
                if (areas[x][y - 4].getKind() == Area.AreaKind.none && areas[x][y - 3].getKind() == Area.AreaKind.none && areas[x][y + 1].getKind() == Area.AreaKind.none && areas[x][y + 2].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FOURTH));
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                /*
                * t
                * x
                * x
                * x <
                * t
                * t
                * */
                if (areas[x][y - 3].getKind() == Area.AreaKind.none && areas[x][y + 1].getKind() == Area.AreaKind.none && areas[x][y + 2].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FOURTH));

                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                /*
                * t
                * t
                * x
                * x
                * x <
                * t
                * */
                if (areas[x][y - 4].getKind() == Area.AreaKind.none && areas[x][y - 3].getKind() == Area.AreaKind.none && areas[x][y + 1].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }

            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkFourPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();


            try {/*
                *  t
                *  x
                *  x
                *  x
                *  x <
                * */
                if (areas[x][y - 4].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y - 4], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }

            try {/*
                *  x
                *  x
                *  x
                *  x <
                *  t
                * */
                if (areas[x][y + 1].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x][y + 1], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }

            return LIST_POINT_TO_BLOCK;
        }
    };
    protected HandlingCase mainDiagonalCase = new HandlingCase() {
        @Override
        public ArrayList<AreaAI> checkTwoPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                /*
                *   t
                *    x
                *     x  <
                *      t
                *       x
                *        t
                * */
                if (areas[x - 2][y - 2].getKind() == Area.AreaKind.none && areas[x - 3][y - 3].getKind() == area.getKind() && areas[x - 4][y - 4].getKind() == Area.AreaKind.none && areas[x + 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 2][y - 2], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    txtxxt
                *        ^
                * */
                if (areas[x + 1][y + 1].getKind() == Area.AreaKind.none && areas[x + 2][y + 2].getKind() == area.getKind() && areas[x + 3][y + 3].getKind() == Area.AreaKind.none && areas[x - 2][y - 2].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    xxtxx
                *     ^
                * */
                if (areas[x + 3][y + 3].getKind() == area.getKind() && areas[x + 2][y + 2].getKind() == area.getKind() && areas[x + 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FIRST));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkThreePoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();

            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                if (areas[x - 4][y - 4].getKind() == area.getKind() && areas[x - 3][y - 3].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y - 3], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x + 2][y + 2].getKind() == area.getKind() && areas[x + 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x - 4][y - 4].getKind() == Area.AreaKind.none && areas[x - 3][y - 3].getKind() == Area.AreaKind.none && areas[x + 1][y + 1].getKind() == Area.AreaKind.none && areas[x + 2][y + 2].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FOURTH));
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x - 3][y - 3].getKind() == Area.AreaKind.none && areas[x + 1][y + 1].getKind() == Area.AreaKind.none && areas[x + 2][y + 2].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FOURTH));

                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x - 4][y - 4].getKind() == Area.AreaKind.none && areas[x - 3][y - 3].getKind() == Area.AreaKind.none && areas[x + 1][y + 1].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 3][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            return LIST_POINT_TO_BLOCK;


        }

        @Override
        public ArrayList<AreaAI> checkFourPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();


            try {

                if (areas[x - 4][y - 4].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 4][y - 4], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x + 1][y + 1].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 1][y + 1], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }

            return LIST_POINT_TO_BLOCK;
        }
    };
    protected HandlingCase extraDiagonalCase = new HandlingCase() {
        @Override
        public ArrayList<AreaAI> checkTwoPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                /*
                *   t
                *    x
                *     x  <
                *      t
                *       x
                *        t
                * */
                if (areas[x + 2][y - 2].getKind() == Area.AreaKind.none && areas[x + 3][y - 3].getKind() == area.getKind() && areas[x + 4][y - 4].getKind() == Area.AreaKind.none && areas[x - 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 2][y - 2], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    txtxxt
                *        ^
                * */
                if (areas[x - 1][y + 1].getKind() == Area.AreaKind.none && areas[x - 2][y + 2].getKind() == area.getKind() && areas[x - 3][y + 3].getKind() == Area.AreaKind.none && areas[x + 2][y - 2].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FOURTH));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            try {
                /*
                *    xxtxx
                *     ^
                * */
                if (areas[x - 3][y + 3].getKind() == area.getKind() && areas[x - 2][y + 2].getKind() == area.getKind() && areas[x - 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FIRST));
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkThreePoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();
            try {
                if (areas[x + 4][y - 4].getKind() == area.getKind() && areas[x + 3][y - 3].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 3][y - 3], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x - 2][y + 2].getKind() == area.getKind() && areas[x - 1][y + 1].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FIRST));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x + 4][y - 4].getKind() == Area.AreaKind.none && areas[x + 3][y - 3].getKind() == Area.AreaKind.none && areas[x - 1][y + 1].getKind() == Area.AreaKind.none && areas[x - 2][y + 2].getKind() == Area.AreaKind.none) {
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FOURTH));
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 3][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x + 3][y - 3].getKind() == Area.AreaKind.none && areas[x - 1][y + 1].getKind() == Area.AreaKind.none && areas[x - 2][y + 2].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FOURTH));

                }
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x + 4][y - 4].getKind() == Area.AreaKind.none && areas[x + 3][y - 3].getKind() == Area.AreaKind.none && areas[x - 1][y + 1].getKind() == Area.AreaKind.none) {

                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 3][y - 3], FOURTH));
                }
            } catch (IndexOutOfBoundsException e) {
            }

            return LIST_POINT_TO_BLOCK;
        }

        @Override
        public ArrayList<AreaAI> checkFourPoint(Area area) {
            LIST_POINT_TO_BLOCK.clear();
            int x = (int) area.getPoint().getX();
            int y = (int) area.getPoint().getY();


            try {

                if (areas[x + 4][y - 4].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x + 4][y - 4], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                if (areas[x - 1][y + 1].getKind() == Area.AreaKind.none)
                    LIST_POINT_TO_BLOCK.add(new AreaAI(areas[x - 1][y + 1], FIRST));
            } catch (IndexOutOfBoundsException e) {
            }

            return LIST_POINT_TO_BLOCK;
        }
    };

    public BaseAI() {
        areas = areaManger.getAreas();
    }

    @Override
    public void addAreaToListAreaOfAI(Area area) {
        listAreaOfAI.add(area);
    }

    @Override
    public void addAreaToListAreaOfUser(Area area) {
        listAreaOfUser.add(area);
    }

    @Override
    public void reset() {
        listAreaOfUser.clear();
        listAreaOfAI.clear();
    }

    protected Area tickRandom(Area area) {
        Area result = null;
        switch (random.nextInt(8)) {
            case 0:
                result = getAboveArea(area);
                break;
            case 1:
                result = getBottomArea(area);
                break;
            case 2:
                result = getLeftArea(area);
                break;
            case 3:
                result = getRightArea(area);
                break;
            case 4:
                result = getTopLeftArea(area);
                break;
            case 5:
                result = getTopRighttArea(area);
                break;
            case 6:
                result = getBottomLeftArea(area);
                break;
            case 7:
                result = getBottomRightArea(area);
                break;
        }
        return result;
    }

    private Area getAboveArea(Area area) {
        try {
            area = areas[(int) area.getPoint().getX()][(int) (area.getPoint().getY() - 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);
            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getBottomArea(Area area) {
        try {
            area = areas[(int) area.getPoint().getX()][(int) (area.getPoint().getY() + 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getLeftArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() - 1)][(int) area.getPoint().getY()];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getRightArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() + 1)][(int) area.getPoint().getY()];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getTopLeftArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() - 1)][(int) (area.getPoint().getY() - 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getTopRighttArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() + 1)][(int) (area.getPoint().getY() - 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getBottomLeftArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() - 1)][(int) (area.getPoint().getY() + 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

    private Area getBottomRightArea(Area area) {
        try {
            area = areas[(int) (area.getPoint().getX() + 1)][(int) (area.getPoint().getY() + 1)];

            if (area.getKind() != Area.AreaKind.none) return tickRandom(area);

            return area;
        } catch (ArrayIndexOutOfBoundsException e) {
            return tickRandom(area);
        }
    }

}
